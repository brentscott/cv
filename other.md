
































Skills {#skills}
--------------------------------------------------------------------------------

- Highly trained in collection and analysis of single molecule laser trapping data

- Experience in isolation and purification of proteins

- Computer programming: proficient in R. Familiar with Python, Nix, Bash, Markdown, Lua, LaTeX, HTML, and CSS
<---



# BACKUP MATERIALS FOR CV

Publications in Progress {data-icon=wrench}
--------------------------------------------------------------------------------

### Tirasemtiv and dATP synergistically reverse the acidosis-induced depression of myosin's force and motion generating capacity

In preparation for journal submission

N/A

2023*

Marang C, Woodward M, **Scott B**, Debold EP

### lasertrapr: Automated analysis of laser trap data

In preparation for journal submission (JOSS) and rOpenSci

N/A

2023*

**Scott B**, Marang C, Woodward M, Debold EP

### Replication, repeatability, and application for utilizing the critical running speed model

In preparation for journal submission (STORK - Communications in Kinesiology)

N/A

2023*

**Scott B**, Knight A, Bertschy M, Hoogkamer W

Abstracts {data-icon=paperclip} 
--------------------------------------------------------------------------------

### A point mutation in switch 1 alters the load dependence of phosphate rebinding to actomyosin

Abstract submitted to 2023 Annual Biophysical Society Meeting

N/A

2022*

Marang C, **Scott B**, Debold EP

### Cardiac myosin's mechanics and load dependent kinetics are not altered by the thin-filament proteins

Abstract submitted to 2023 Annual Biophysical Society Meeting

N/A

2023*

**Scott B\* **, Clippinger S*, Barrick S, Stump W, Blackwell T, Greenberg MJ

### Cardiac myosin velocity and force are dramatically improved with an alternate triphoshate substrate

Abstract submitted to 2023 Annual Biophysical Society Meeting

N/A

2023*

Woodward M, Marang C, **Scott B**, Debold EP

### Fitting a model to multiscale data suggests thick filament activation can produce force depression in muscle fibers

Abstract submitted to 2023 Annual Biophysical Society Meeting

N/A 

2023*

Walcott S, Marang C, **Scott B**, Woodward M, Debold EP


